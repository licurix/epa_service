 <?php
require_once('pdf/fpdf.php');   // Include clasa FPDF
include("connect.php");

if(isset($_POST['id']))
{
//interogari cu baza de date
$rez = mysql_query("SELECT * FROM factura WHERE id_factura = '".$_POST['id']."'");
$nr_row = mysql_num_rows($rez);

$nr_factura = "";
$data = "";
$id_comanda = "";
$nr_aviz="";
$delegat="";
$buletin="";
$eliberat="";
$nr_masina="";

$row=mysql_fetch_array($rez);

	$nr_factura  = $row['nr_factura'];
	$data     	 = $row['dataf'];
	$id_comanda  = $row['id_comanda'];
	$nr_aviz  	 = $row['nr_aviz'];
	$delegat 	 = strtoupper($row['delegat']);
	$buletin  	 = strtoupper($row['buletin']);
	$eliberat 	 = strtoupper($row['eliberat']);
	$nr_masina   = strtoupper($row['nr_masina']);


$rez_client = mysql_query("SELECT * FROM clienti as cl, comenzi as c WHERE (c.id_client=cl.id_client)AND(c.id='".$id_comanda."') ");

$nume  = "";
$oras  = "";
$adresa= "";
$cui="-";
$nr_ordine="-";
$cont="-";
$banca="-";
$strac="";
$nr_stc="";
$blc="";
$scc="";
$apc="";
$nr_comanda="";
	$row_c=mysql_fetch_array($rez_client);
	$nr_comanda=$row_c['nrcomanda'];
	$nume   = strtoupper($row_c['nume']);
	$oras   = strtoupper($row_c['oras']);
	$strac  = strtoupper($row_c['strada']);
	$nr_stc = strtoupper($row_c['nr']);
	$blc =strtoupper($row_c['bl']);
	$scc =strtoupper($row_c['sc']);
	$ac =$row_c['ap'];
	if($strac!="")
		$strac = "str.".$strac;
	if($nr_stc!="")
		$nr_stc =", nr.".$nr_stc;
	if(strlen($blc)>0 && $blc!="-")
		$blc = ", bl.".$blc;
	if(strlen($scc)>0 && $scc !="-")
		$scc =", sc.".$scc;
	if(strlen($apc)>0 && $apc!="-")
		$apc =", ap.".$apc;
	$adresa = $strac." ".$nr_stc." ".$blc." ".$scc." ".$apc;
	$cui    = strtoupper($row_c['cui']);
	$nr_ordine=strtoupper($row_c['nr_ordine']);
	$banca  = strtoupper($row_c['banca']);
	$cont   = strtoupper($row_c['cont']);

$sql_masina = mysql_query("SELECT * FROM masini WHERE id_masina ='".$row_c['id_masina']."'");
$row_masina = mysql_fetch_array($sql_masina);
if($nr_masina =="")
	$nr_masina = strtoupper($row_masina['nr_inmat']);

//interogari pt lucrari efectuate
$sql_lucrari = mysql_query("SELECT * FROM operatii WHERE (nr_comanda = '".$id_comanda."')");

$timp_l="";
$ni=0;

while($row_lucrari = mysql_fetch_array($sql_lucrari))
{
	$timp_l[$ni]    = $row_lucrari['timp_normat'];
	$ni++;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`interogari pt piese
$sql_piese = mysql_query("SELECT * FROM piese_comanda WHERE (id_comanda='".$id_comanda."') ");
$nr_piese = mysql_num_rows($sql_piese);

$valoare_p="";
$pi=0;

while($row_piese=mysql_fetch_array($sql_piese))
{
	$valoare_p[$pi]=$row_piese['pret']*$row_piese['buc'];
	$pi++;
}

$total_op=0;//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`total operatii
for($i=0;$i<$ni;$i++)
	$total_op+=$timp_l[$i]*$ora_manopera;

$total_p=0;//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`total piese
for($j=0;$j<$pi;$j++)
	$total_p +=$valoare_p[$j];

$total = round($total_op+$total_p,2);
$tva = round($total*$TVA,2);
$tot = round($total+$tva,2);

$nr_chitanta="";
$valoare="";
$serie="";
$sql_chitanta = mysql_query("SELECT * FROM  chitanta  WHERE id_factura='".$_POST['id']."'");
$row_chitanta =mysql_fetch_array($sql_chitanta);
$nr_chitante = mysql_num_rows($sql_chitanta);
	$valoare = $row_chitanta['valoare'];
	$serie = $row_chitanta['serie'];
	$nr_chitanta = $row_chitanta['nr_chitanta'];

mysql_close();


// Creaza documentul de lucru
$pdf=new FPDF('P', 'mm', 'A4' );

//$pdf->setSourceFile("factura_fiscala.pdf");
$pdf->Open();

$pdf->SetAuthor('S.C. WinNido S.R.L.');
$pdf->SetCreator('S.C. WinNido S.R.L.');
$pdf->SetTitle('Factura fiscala');
$pdf->SetSubject('Factura pdf');
$pdf->SetKeywords('Factura tiparita');

// Initializeaza pagina si defineste fontul textului
$pdf->AddPage();
$pdf->SetFont('Arial','B',32);
$pdf->AliasNbPages(); 


$pdf->Image('pdf/logo.png',30,70,150);
$pdf->SetXY(9,8);
$pdf->SetLineWidth(0.01);
$pdf-> Line(95,35,127,35); //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`imi face linii la nr facturii
$pdf-> Line(108,39,127,39); //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`imi face linii la data
$pdf-> Line(113,43,127,43); //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`imi face linii la nr facturii
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~LINIILE DE LA cumparator

//$pdf-> Line(148,13,195,13);
$pdf-> Line(131,18,195,18);//nume
$pdf-> Line(157,23,195,23);
$pdf-> Line(140,28,195,28);
$pdf-> Line(141,33,195,33);
$pdf-> Line(131,38,195,38);
$pdf-> Line(142,43,195,43);
$pdf-> Line(141,48,195,48);
$pdf-> Line(141,53,195,53);//banca
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~```liniile de la date privind expeditia
$pdf-> Line(79,118,138,118);//delegat
$pdf-> Line(63,121.5,87,121.5);//seria
//$pdf-> Line(85,121.5,100,121.5);//nr
$pdf-> Line(99,121.5,138,121.5);//eliberat
$pdf-> Line(80,124.5,138,124.5);//mujloc transport
$pdf-> Line(63,130.5,100,130.5);//data
$pdf-> Line(106,130.5,138,130.5);//ora
$pdf-> Line(68,133.5,138,133.5);//semnatura

$pdf->SetLineWidth(0.4);//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~imi ingroasa chenarele tabelelor

$pdf->cell(190,127,"",1,0,false);//chenarul facturii

$pdf->SetFont('Arial','',8);
// Sirul ce contine textul cu mai multe linii, care va fi adaugat in metoda Write()

$firma = $nume_firma."".$sediu_firma."".$punct_lucru."".$banca_firma."".$contact_firma;

$pdf->SetXY(10,10);
$pdf->WriteHTML($firma); // Paragraf cu inaltimea liniei de 4 mm
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~` end firma
$pdf->SetXY(87,10);
$pdf->WriteHTML("Seria: <b>".$SERIE_FACTURA."</b>   nr. ".$nr_factura);

///~~~~~~~~~~~~~~~~~~~~~~~~~~`date cumparator
$pdf->SetXY(150,10);

$pdf->WriteHTML("Cumparator: ");
$pdf->SetXY(130,15);
//$pdf->WriteHTML($nume);
$pdf->cell(60,3,$nume,0,0,'C');
$pdf->SetXY(130,20);
$nrord="Nr.ord.reg.com/an:  $nr_ordine";
//$pdf->Cell( 0, 15, $comand, 0, 0, 'L' );
$pdf->WriteHTML($nrord);
$pdf->SetXY(130,25);
$cif="C.I.F.:  $cui";
//$pdf->Cell( 0, 15, $coman, 0, 0, 'L' );
$pdf->WriteHTML($cif);
$pdf->SetXY(130,30);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`37 de caractere pe rand
$adresa1 = substr($adresa,0,37);
$adresa2 = substr($adresa,37);
$sediu="Sediul:  ".$adresa1;
$pdf->WriteHTML($sediu);
$pdf->SetXY(130,35);
$pdf->WriteHTML($adresa2);
$pdf->SetXY(130,40);
$judet="Judetul:  ".$oras;
$pdf->WriteHTML($judet);
$pdf->SetXY(130,45);
$conte="Contul:  ".$cont;
$pdf->WriteHTML($conte);
$pdf->SetXY(130,50);
$bancae="Banca:  ".$banca;
$pdf->WriteHTML($bancae);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~factura fiscala
$pdf->Ln();
$pdf->SetFont('','B',12);
$pdf->SetXY(90,20);
$nota = strtoupper("Factura");
$pdf->Write(1,$nota);
$pdf->Ln();
$pdf->SetFont('','B',12);
$pdf->SetXY(91,25);
$pdf->Write(1,"FISCALA");
$pdf->Ln(1);
$pdf->SetXY(78,30);
$pdf->SetFont('Arial','',9);
$pdf->cell(50,15,"",1,0,false);
$pdf->SetXY(78,31);
$pdf->cell(50,5,"Nr. facturii        ".$nr_factura,0,0);

$pdf->SetXY(78,35);
$pdf->cell(50,5,"Data(ziua,luna,anul) ".$data,0,0);
$pdf->SetXY(78,39);
$pdf->cell(50,5,"Nr. aviz insotire a marfii ".$nr_aviz,0,0);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~produse si sevicii+valoare si tva
///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~date despre client

//$pdf->WriteHTML($Proprietar);
$pdf->SetXY(9,58);
$pdf->cell(10,10,"Nr.crt",1,0,'L');

$pdf->SetXY(19,58);
$pdf->cell(94,10,"Denumirea produselor sau a serviciilor",1,0,'C');

$pdf->cell(10,10,"U.M.",1,0);
//$pdf->SetXY(129,61);

$pdf->cell(16,10,"Cantitatea",1,0);
//$pdf->Ln(7);

$pdf->cell(20,10,"",1,0);
$pdf->SetXY(139,59);
$pdf->WriteHTML("Pretul unitar");
$pdf->SetXY(140,62);
$pdf->WriteHTML("(fara T.V.A)");
$pdf->SetXY(143,65);
$pdf->WriteHTML("-RON-");

$pdf->SetXY(159,58);
$pdf->cell(20,10,"",1,0);
$pdf->SetXY(161,60);
$pdf->WriteHTML("Valoarea");
$pdf->SetXY(163,63);
$pdf->WriteHTML("-RON-");

$pdf->SetXY(179,58);
$pdf->MultiCell(20,10,"",1,0);
$pdf->SetXY(182,59);
$pdf->WriteHTML("Valoarea");
$pdf->SetXY(184,62);
$pdf->WriteHTML("T.V.A");
$pdf->SetXY(184,65);
$pdf->WriteHTML("-RON-");
$pdf->Ln();

$pdf->SetXY(9,68);
$pdf->cell(10,4,"0",1,0,'C',false);
$pdf->cell(94,4,"1",1,0,'C',false);
$pdf->cell(10,4,"2",1,0,'C',false);
$pdf->cell(16,4,"3",1,0,'C',false);
$pdf->cell(20,4,"4",1,0,'C',false);
$pdf->cell(20,4,"5(3x4)",1,0,'C',false);
$pdf->cell(20,4,"6",1,0,'C',false);
$pdf->Ln();
$pdf->SetXY(12,73);
$pdf->WriteHTML("1");
$pdf->SetX(20);
$pdf->WriteHTML("Servicii conform devizului de reparatie nr ".$nr_comanda);
$pdf->SetX(113);
$pdf->WriteHTML("BUC");
$pdf->SetX(129);
$pdf->WriteHTML("1");
$nr1 = strlen($total);
$pdf->SetX(148-$nr1);
$pdf->WriteHTML($total);
$pdf->SetX(168-$nr1);
$pdf->WriteHTML($total);
$nr2=strlen($tva);
$pdf->SetX(188-$nr2);
$pdf->WriteHTML($tva);


$pdf->SetXY(9,68);
$pdf->cell(10,44,"",1,0,'C',false);
$pdf->cell(94,44,"",1,0,'C',false);
$pdf->cell(10,44,"",1,0,'C',false);
$pdf->cell(16,44,"",1,0,'C',false);
$pdf->cell(20,44,"",1,0,'C',false);
$pdf->cell(20,44,"",1,0,'C',false);
$pdf->cell(20,44,"",1,0,'C',false);

if($nr_chitante<1)
{
	$pdf->SetXY(146,107);
	$pdf->WriteHTML("<b>Factura  se achita cu  O.P.</b>");
	$pdf->Ln(2);
}

$pdf->Ln();
$pdf->SetX(9);
$pdf->cell(40,23,"",1,0,false);
$pdf->SetXY(13,115);
$pdf->WriteHTML("Semnatura si stampila <br>        furnizorului");
$pdf->SetXY(50,112);
$pdf->WriteHTML("Date privind expeditia:");
$pdf->SetXY(50,115.5);
$pdf->WriteHTML("Numele delegatului ".$delegat);
$pdf->SetXY(50,119);
$pdf->WriteHTML("C.I./B.I.: ".$buletin);

$pdf->SetX(87);
$pdf->WriteHTML("Eliberat ".$eliberat);
$pdf->SetXY(50,122);
$pdf->WriteHTML("Mijlocul de transport ".$nr_masina);
$pdf->SetXY(50,125);
$pdf->WriteHTML("Expedierea s-a efectuat in prezenta noastra la");
$pdf->SetXY(50,128);
$pdf->WriteHTML("data de ".$dataf);
$pdf->SetX(100);
$pdf->WriteHTML("ora");
$pdf->SetXY(50,131);
$pdf->WriteHTML("Semnaturile");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`casute cu total
$pdf->SetXY(139,112);
$pdf->cell(20,10,"Total",1,0);
$pdf->cell(20,10,$total,1,0,'C');
$pdf->cell(20,10,$tva,1,0,'C');
$pdf->SetXY(139,122);
$pdf->cell(20,13,"",1,0);
$pdf->SetXY(140,123);
$pdf->WriteHTML("Semnatura");
$pdf->SetXY(142,126);
$pdf->WriteHTML("primire");
$pdf->SetXY(160,123);
$pdf->WriteHTML("Total de plata");
$pdf->SetXY(160,126);
$pdf->WriteHTML("<sub>(col.5+col.6)</sub>");
$pdf->SetXY(180,129);
$pdf->WriteHTML("<center>".$tot."</center>");

$pdf->Ln(20);
$pdf->SetFont('Arial','',7);
if($nr_chitante>0)
{
	
	$pdf->SetXY(9,148);
	
	$pdf->cell(190,70,"",1,0,false);//chenarul facturii
	
	$pdf->SetX(20);
	$pdf->WriteHTML("<b>S.C. WIN NIDO S.R.L.</b>");
	$pdf->Ln(4);
	$pdf->SetX(10);
	$pdf->WriteHTML("Nr.Ord.Reg.Com./an<b>J08/977/09.09.2010</b>");
	$pdf->Ln(4);
	$pdf->SetX(10);	
	$pdf->WriteHTML("Cod de inregistrare fiscala: <b>RO27366713</b>");
	$pdf->Ln(4);
	$pdf->SetX(10);
	$pdf->WriteHTML($sediu_firma);
	$pdf->SetFont('Arial','',9);
	$pdf->Ln(4);
	$pdf->SetX(80);
	$pdf->WriteHTML("CHITANTA   Seria: <b>".$serie."</b>    Nr.: <b>".$nr_chitanta."</b>");
	
	$pdf->Ln(4);
	$pdf->SetX(90);
	$pdf->WriteHTML("Data: <b>".$data."</b>");
	$pdf->Ln(4);
	$pdf->SetX(10);
	$pdf->WriteHTML("Am primit de la <b>".$nume."</b>");
	$pdf->Ln(4);
	//$pdf->SetX(10);
	$pdf->WriteHTML("Nr.Reg.Com/an  <b>".$nr_ordine."</b>");
	//$pdf->SetX(80);
	$pdf->WriteHTML(" C.I.F. <b>".$cui."</b>");
	$pdf->Ln(4);
	//$pdf->SetX(10);
	$pdf->WriteHTML("Adresa  <b>".$oras." ".$adresa."</b>");
	$pdf->Ln(4);
	//$pdf->SetX(10);
	$pdf->WriteHTML("Suma de <b>".round($valoare,2)." lei (".strLei($valoare,'','.').")"."</b>");
	//$pdf->SetX(62);
	//$pdf->cell(60,10,"(".strLei($valoare,'','.').")");
	$pdf->Ln(4);
	
	$reprezinta ="reprezentand  plata partiala la factura nr <b>".$nr_factura."</b> din data <b>".$data."</b>";
	if($valoare == $tot)
		$reprezinta ="reprezentand  contravaloare factura nr <b>".$nr_factura."</b> din data <b>".$data."</b>";
	//$pdf->SetX(10);
	$pdf->WriteHTML($reprezinta);
	$pdf->Ln(4);
	$pdf->SetX(160);
	$pdf->cell(60,10,"CASIER,");
	/*$pdf->SetLineWidth(0.01);
	$pdf-> Line(53,183,185,183);//LINIE NUME
	$pdf-> Line(54,187,185,187);//reg
	$pdf-> Line(42,191,185,191);//adresa
	$pdf-> Line(45,195,185,195);//suma
	$pdf-> Line(50,199,185,199);//data*/
}//end if chitanta


$pdf->Output("$nrauto",'D');                         // Trimite datele de iesire la browser
}
?> 

