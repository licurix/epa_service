<?php
	include("connect.php");
		
?>
<html>

<HEAD><title>WinNido</title>
<link rel="stylesheet" href="style.css" media="screen">
<script language="javascript" src="function.js"></script>
<script language="javascript" src="jscript.js"></script>

<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

</HEAD>
<body onunload="opener.location.reload()">
<script>
function formValid()
{
	var nr = 		 document.getElementById('nr');
	var model =		 document.getElementById('model');
	var vin = 		 document.getElementById('vin');
	var tip_motor  = document.getElementById('tip_motor');
	var proprietar = document.getElementById('details');
	var marca = 	 document.getElementById('marca');

if(nr.value.length==0)
	{
		alert("Completati numarul de inmatriculare");
		nr.focus();
		return false;
	}
else
	if(marca.value=='null')
		{
			alert("Selectati marca masinii");
			marca.focus();
			return false;
		}
else
	if(model.value.length==0)
		{
			alert("Completati modelul masinii");
			model.focus();
			return false;
		}
	

	else
		if(vin.value.length==0)
			{
				alert("Completati V.I.N.");
				vin.focus();
				return false;
			}
		else

			if(tip_motor.value.length==0)
			{
				alert("Completati tipul motorului");
				tip_motor.focus();
				return false;
			}
			else
				if(proprietar.value.length==0)
				{
					alert("Selectati proprietar auto");
					proprietar.focus();
					return false;
				}

	return true;
}
</script>
<?php
	$value_bt="Adauga";
	
	$id_client=0;
if(isset($_GET['idm']))
	{
		$value_bt="Modifica";
		$sql_m = mysql_query("SELECT * FROM masini WHERE id_masina='".$_GET['idm']."'");
		$row_m = mysql_fetch_array($sql_m);
		$nr_inmat    = strtoupper($row_m['nr_inmat']);
		$categ		 = $row_m['categ'];
		$marcaa		 = $row_m['marca'];
		$model		 = strtoupper($row_m['model']);
		$an			 = $row_m['an'];
		$vin		 = strtoupper($row_m['vin']);
		$tipmotor    = $row_m['tipmotor'];
		$cap		 = $row_m['cap'];
		$kw			 = $row_m['kw'];
		$combustibil = $row_m['combustibil'];
		$sql_id = mysql_query("SELECT * FROM clienti as c, client_masina as cm WHERE c.id_client = cm.id_client AND cm.id_masina='".$_GET['idm']."'");
		$row_id_client = mysql_fetch_array($sql_id);
		$id_client = $row_id_client['id_client'];
	}
?>
<div class="tot">
</div>
	<div class="content">
	<div class="title"><?php echo $value_bt;?> masina</div>

<div class="form" style="width:100%;margin-top:15px;;margin-left:70px;">
<input type='hidden' id='oras'>
<input type='hidden' id='tel'>
	<form action="add_masina.php" method="POST" >
	<?php
	if(isset($id_client) && isset($_GET['idm']) && $id_client>0)
	{
		?>
		<input type='hidden' name='id_cliente' value="<?php echo $id_client;?>">
		<input type='hidden' name='id_masine' value="<?php echo $_GET['idm'];?>">
		<?php
	}
	?>
	
		<div  >
			<div >
				<div style='margin-top:15px'>Nr inmat <input type="text" style='margin-left:15px' id='nr' name="nr"
				<?php
					if(isset($nr_inmat))
						echo 'value="'.$nr_inmat.'"';
					else
						echo 'value =""' ;
				?>
				class="text" size="8" onblur="toUppercase(this)"></div><br>
			</div>
			<div>
				<div>Categorie <select name="categ" style='margin-left:10px' class="text">
				<?php
					if(isset($_GET['idm']))
						echo "<option value='".$categ."'>".ucwords($categ)."</option>";
					$cat=array("berlina","cabrio","coupe","break","hatchback","pickup","off- road","van/ minibus","monovolum","suv");
					for($i=0;$i<count($cat);$i++)
						echo "<option value='".$cat[$i]."'>".ucwords($cat[$i])."</option>";
				?>
					</select>
				</div>
			</div><br>
			<div >
				<div>Marca<select name="marca" style='margin-left:32px' class="text" id='marca'>
					
					<?php
					if(isset($_GET['idm']))
						echo "<option value='".$marcaa."'>".ucwords($marcaa)."</option>";
						else
							echo "<option value='null'>--selecteaza--</option>";
					$marca_auto=array("alfa romeo","aro","audi","bmw","chevrolet","crysler","citroen","dacia",
						"daewoo","daihatsu","daimler","dodge","fiat","ford","gaz","honda","hyundai","isuzu","jeep","jaguar"
						,"kia","lada","lancia","land rover","lexus","mazda","mercedes benz","mini","mitsubishi"
						,"nissan","oltcit","opel","peugeot","porsche","renault","rover","saab","seat","skoda","smart","ssangyoung"
						,"subaru","suzuki","toyota","trabant","vauxhall","volkswagen","volvo");
						for($i=0;$i<count($marca_auto);$i++)
							echo "<option value='".$marca_auto[$i]."'>".ucwords($marca_auto[$i])."</option>";
					?>
					</select>
				<span style='margin-left:50px'>Model 
				<?php
					if(isset($model))
						echo '<input type="text" name="model" id="model" class="text" size="30" onblur="toUppercase(this)" value="'.$model.'"> ';
					else
						echo '<input type="text" name="model" id="model" class="text" size="30" onblur="toUppercase(this)" value=""> ' ;
				?>
				</div>
			</div><br>
			<div class='row'>
				<div>An   <select name="an" class="text" style='margin-left:47px'>
					<?php
					if(isset($_GET['idm']))
						echo "<option value='".$an."'>".$an."</option>";
						$data=date('Y');
						for($i=$data;$i>=1980;$i--)
							echo "<option value='".$i."'>".$i."</option>";
					?>
					</select>
					<span style='margin-left:120px'>	V.I.N
					<?php
					if(isset($vin))
						echo '<input type="text" name="vin" id="vin" class="text" onblur="toUppercase(this)" value="'.$vin.'" size="30"> ';
					else
					{
					?>
						<input type="text" name="vin" id="vin" class="text" onblur="toUppercase(this)" size='30'>
				<?php
					}
				?>
					</div><br>
			</div>
			<div class='row'>
					<div>Tip motor<input type="text" style='margin-left:13px' id='tip_motor' name="tipmotor" onblur="toUppercase(this)"
					<?php
					if(isset($tipmotor))
						echo 'value="'.strtoupper($tipmotor).'"';
					else
						echo 'value =""' ;
				?>
					class="text" size="12"></div>
			</div><br>
			<div class='row'>
					<div>Proprietar

						<?php
						if(isset($_GET['id']))
						{
							$id_client = $_GET['id'];
							$prop=mysql_query("SELECT * FROM clienti WHERE id_client ='".$id_client."'");
							$row_c = mysql_fetch_assoc($prop);

								echo "<input type='text'  id='details' name='nume' class='text' size='24' readonly title='Click pe butonul Client pt a alege un client' value='".ucwords($row_c["nume"])."'>";
								echo "<input type='hiDden' name='proprietar' id='details' value='".$id_client."'>";
						}
						else{
								$prop=mysql_query("SELECT * FROM clienti WHERE id_client ='".$id_client."'");
								$row_c = mysql_fetch_assoc($prop);
								echo "<input type='text' style='margin-left:7px' id='details' name='nume' class='text' size='24' readonly title='Click pe butonul Client pt a alege un client' value='".ucwords($row_c["nume"])."'>";

								echo "<input type='hidden' class='text' name='proprietar' id='id_cl' value='".$id_client."'>";
							if(!isset($_GET['idm']))
							{
							?>
							<input type="button" style='margin-left:5px' name="choice" title='Click pentru lista clientilor existenti'value="Client" id='bt' class='text albastru' onClick="openWindow('clienti.php')" >
						<?php }} ?>
				</div>
			</div><br>
			<div class='row'>
				<div>Capacitate <input type='text' name="capacitate" class="text" size='8'
				<?php
				if(isset($_GET['idm']))
						echo " value='".ucwords($cap)."' >";
				else
					echo "value=''>";
				/*	$cap = array("1600","1400","1500","700","900","1100","1200","1700","1800","1900","2000","2500","3000","4000",
					"5000","6000");
					for($i=0;$i<count($cap);$i++)
						echo "<option value='".$cap[$i]."'>".$cap[$i]."</option>";
				*/?>
				cm<sup>3</sup>
				<span style='margin-left:50px'>KW <input type='text' name="kw" class="text" size='8'
				<?php
				if(isset($_GET['idm']))
						echo "value='".$kw."'";
					else
						echo "value=''";
				/*	$kwi=array("74 kw /100 cp","37 kw /50 cp","44 kw /60 cp","59 kw /80 cp","92 kw /125 cp","110 kw /150 cp",
					"147 kw /200 cp","221 kw /300 cp","294 kw /400 cp","368 kw /500 cp");

					for($i=0;$i<count($kwi);$i++)
					{
						echo "<option value='".$kwi[$i]."'>".$kwi[$i]."</option>";
					}
				*/
				?>
				>
				<span style='margin-left:30px'>Combustibil <select name="combustibil" class="text">
				<?php
				if(isset($_GET['idm']))
						echo "<option value='".$combustibil."'>".ucfirst($combustibil)."</option>";
					$comb=array("diesel","benzina","gpl","hibrid","electrica");;
					for($i=0;$i<count($comb);$i++)
						echo "<option value='".$comb[$i]."'>".ucwords($comb[$i])."</option>";
				?>
					</select>
				</div>
			</div>
			<div class='row'>
				<div>
				<input type="submit" style='margin-left:150px;margin-top:30px;'name="trimite" value="<?php echo $value_bt;?>" class="text" id='bt'  onClick="return formValid()">
			<!--	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" value="Sterge" class="text" onClick="this.form.reset()" id='bt'>
			-->
				<input type='button' value="Inchide"  style='margin-left:50px'class='text rosu' id='bt' onClick="javascript:self.close()" title='Inchide fereastra'><br><br>
				 <sub>- toate campurile sunt obligatorii</sub>
				</div><br><br>
			</div>
		</div>
	</form>
	<p>
</div><br>
<div id='winnido'>WinNido</div>