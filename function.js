function confirmDelete(delUrl,mes) {
  if (confirm("Sunteti sigur ca vreti sa stergeti "+mes+" ?"))
	{
		document.location = delUrl;
	}
}
function filter (term, _id, cellNr){
	var suche = term.value.toLowerCase();
	var table = document.getElementById(_id);
	var ele;
	for (var r = 1; r < table.rows.length; r++){
		ele = table.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g,"");
		if (ele.toLowerCase().indexOf(suche)>=0 )
			table.rows[r].style.display = '';
		else 
		  {
			  table.rows[r].style.display = 'none';
		  }
	}
}
var state = 'none';

function showhide(layer_ref) {

if (state == 'block') {
state = 'none';
}
else {
state = 'block';
}
if (document.all) { //IS IE 4 or 5 (or 6 beta)
eval( "document.all." + layer_ref + ".style.display = state");
}
if (document.layers) { //IS NETSCAPE 4 or below
document.layers[layer_ref].display = state;
}
if (document.getElementById &&!document.all) {
hza = document.getElementById(layer_ref);
hza.style.display = state;
}
} 

function delayer(locatie){
    window.location = locatie;
}

function toUppercase(elem)
{	
	tmp = elem.value.toUpperCase();
	elem.value=tmp;
}

function switchid(id){
	var elem = document.getElementById("tip");
	select =elem.selectedIndex;
	//alert(select);
	if(select=="0")
		showdiv(id);
		else
			hidediv(id);
}

function showdiv(id) {
	//safe function to show an element with a specified id
		  
	if (document.getElementById) { // DOM3 = IE5, NS6
		document.getElementById(id).style.display = 'block';
	}
	else {
		if (document.layers) { // Netscape 4
			document.id.display = 'block';
		}
		else { // IE 4
			document.all.id.style.display = 'block';
		}
	}
}

function hidediv(id) {
	//safe function to hide an element with a specified id
	if (document.getElementById) { // DOM3 = IE5, NS6
		document.getElementById(id).style.display = 'none';
	}
	else {
		if (document.layers) { // Netscape 4
			document.id.display = 'none';
		}
		else { // IE 4
			document.all.id.style.display = 'none';
		}
	}
}

function checkLetter(elem, msg){
  var alph = /^[a-zA-Z ]+$/;
  if(elem.value.match(alph)){
     return true;
  }else{
	 elem.value="";
     elem.focus();
     return false;
  }
}
function checkNumber(elem,msg)
{
var re = /^[0-9-'.']*$/;
if (!re.test(elem.value)) {
elem.focus();
elem.value = elem.value.replace(/[^0-9+.+0-9]/,"");

}
  /*var alph =/^[0-9-'.'-',']*$/;
  if(elem.value.match(alph)){
	return true;
	}else{
		alert(msg);
		elem.value="";
		
		return false;
	}*/
 }
function isCnp(elem,mess)
{
	consta = new String("279146358279");
	var cnp = elem.value;
	
	if(cnp.length !=13 ){
		alert('Cnp 13 cifre');
		
		elem.value='';
		return false;
	}
	if(cnp.substr(3,2)>12){
		alert('luna incorecta');
	
		elem.value='';
		return false;
	}
	if(cnp.substr(5,2)>31){
		alert('ziua incorecta');

		elem.value='';
		return false;
	}

	suma =0;
	for(i=0;i<consta.length;i++)
		suma = suma+cnp.charAt(i)*consta.charAt(i);
	rest = suma%11;
	if( (rest<10 && rest == cnp.charAt(12) ) || (rest == 10 && cnp.charAt(12) ==1 ) )
		return true;
	else{
		alert(mess);

		elem.value='';
		return false;
	}
	return false;
}
function setfocus(){
    document.forms[0].nume.focus()
}

/*telefon*/
var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 14;
var phonevalue1;
var phonevalue2;
var cursorposition;

function ParseForNumber1(object){
  phonevalue1 = ParseChar(object.value, zChar);
}

function ParseForNumber2(object){
  phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object,e) {
  if(e){
    e = e
  } else {
    e = window.event
  }
  if(e.which){
    var keycode = e.which
  } else {
    var keycode = e.keyCode
  }

  ParseForNumber1(object)

  if(keycode >= 48){
    ValidatePhone(object)
  }
}

function backspacerDOWN(object,e) {
  if(e){
    e = e
  } else {
    e = window.event
  }
  if(e.which){
    var keycode = e.which
  } else {
    var keycode = e.keyCode
  }
  ParseForNumber2(object)
}

function GetCursorPosition(){

  var t1 = phonevalue1;
  var t2 = phonevalue2;
  var bool = false
  for (i=0; i<t1.length; i++)
  {
    if (t1.substring(i,1) != t2.substring(i,1)) {
      if(!bool) {
        cursorposition=i
        window.status=cursorposition
        bool=true
      }
    }
  }
}
function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
       return true;
    }
	
	var myWindow;
function openWindow(url)
{
	if (window.showModalDialog) 
		myWindow = window.showModalDialog(url,"_blank","dialogWidth:820px;dialogHeight:600px;status:no;location:no;center:yes;");
	else 
		 myWindow=window.open(url,"_blank",'height=600,width=820,titlebar=no,unadorned =yes;location=no,copyhistory=no,toolbar=no,directories=0,status:no,linemenubar=no,scrollbars=no,resizable=no');
		
}
function closeWindow()
{
	myWindow.close();
}

