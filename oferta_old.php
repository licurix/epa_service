<?php
require('pdf/fpdf.php');   // Include clasa FPDF
include("connect.php");

if(isset($_POST['id_c']))
{
//interogari cu baza de date
$rez = mysql_query("SELECT * FROM oferta WHERE id_oferta = '".$_POST['id_c']."'");
$nr_row = mysql_num_rows($rez);


$data = "";
$id_masina="";
$solicit="";

$row=mysql_fetch_array($rez);

	$data      = $row['datao'];
	$id_masina= $row['id_masina'];
	$solicit = strtoupper($row['solicit_o']);
	
$rez_mas = mysql_query("SELECT * FROM masini as m ,clienti as c, client_masina as cm WHERE (m.id_masina = '".$id_masina."')AND(m.id_masina=cm.id_masina)AND(cm.id_client=c.id_client) ");
$row_m=mysql_fetch_array($rez_mas);
	$id_client = $row_m['id_client'];
$rez_client = mysql_query("SELECT * FROM clienti WHERE id_client='".$id_client."' ");

$numec    = "";
$oras ="";
$tel ="";

$row_c=mysql_fetch_array($rez_client);
	
	$numec    = strtoupper($row_c['nume']);
	$oras    = strtoupper($row_c['oras']);
	$tel    = strtoupper($row_c['tel']);


$marca ="";
$nrauto="";
$vin="";
$tip="";
$an="";
$serie="";

$marca  = strtoupper($row_m['marca']);
$nrauto = strtoupper($row_m['nr_inmat']);
$vin    = strtoupper($row_m['vin']);
$tip    = strtoupper($row_m['categ']);
$serie  = strtoupper($row_m['cap']."/".$row_m['kw']."/".$row_m['tipmotor']);
$an     = $row_m['an'];

//interogari pt lucrari efectuate
$sql_lucrari = mysql_query("SELECT * FROM oferta_operatii WHERE (id_oferta = '".$_POST['id_c']."')");
$nr_lucrari = mysql_num_rows($sql_lucrari);

$operati_l="";
$timp_l="";

$n=0;
$nr_operatii = mysql_num_rows($sql_lucrari);
while($row_lucrari = mysql_fetch_array($sql_lucrari))
{

	$operati_l[$n] = strtoupper($row_lucrari['operatie']);
	$timp_l[$n]    = $row_lucrari['timp_normat'];

	$n++;
}

$sql_piese = mysql_query("SELECT * FROM oferta_piese WHERE (id_oferta='".$_POST['id_c']."') ");
$nr_piese = mysql_num_rows($sql_piese);

$numepiese="";
$cantitate_p="";
$valoare_p="";
$pi=0;

while($row_piese=mysql_fetch_array($sql_piese))
{
	$numepiese[$pi]=strtoupper($row_piese['piesa']);
	$cantitate[$pi]=$row_piese['buc'];
	$valoare[$pi]=$row_piese['pret'];
	$pi++;
}

mysql_close();


// Creaza documentul de lucru
$pdf=new FPDF('P', 'mm', 'A4' );
$pdf->Open();

$pdf->SetAuthor('S.C. WinNido S.R.L.');
$pdf->SetCreator('S.C. WinNido S.R.L.');
$pdf->SetTitle('Oferta pret');
$pdf->SetSubject('Oferta pret pdf');
$pdf->SetKeywords('Oferta tiparita');

// Initializeaza pagina si defineste fontul textului
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->AliasNbPages(); 
$image="IMAGES/SIGLA.PNG";
//$pdf->Image($image,0,50,50);
$pdf->Cell( 40, 40, $pdf->Image($image, 130, 32, 50,20), 0, 0, 'L', false );

// Sirul ce contine textul cu mai multe linii, care va fi adaugat in metoda Write()
$firma = $nume_firma."".$sediu_firma."".$punct_lucru."".$banca_firma."".$contact_firma;
$pdf->SetXY(20,10);
$pdf->WriteHTML($firma); // Paragraf cu inaltimea liniei de 4 mm
/*
$pdf->SetXY(140,10);
$comanda =  "Nr. comanda: <b>$nr_comanda</b>";
//$pdf->Cell( 0, 15, $comanda, 0, 0, 'L' );
$pdf->WriteHTML($comanda);
*/
$pdf->SetXY(140,15);
$comand="Data: <b>$data</b>";
//$pdf->Cell( 0, 15, $comand, 0, 0, 'L' );
$pdf->WriteHTML($comand);
/*
$pdf->SetXY(140,20);
$coman="Termen reparatie: <b>$termen</b>";
//$pdf->Cell( 0, 15, $coman, 0, 0, 'L' );
$pdf->WriteHTML($coman);
$pdf->SetXY(140,25);
$coma="Termen modificat:";
$pdf->WriteHTML($coma);
//$pdf->Cell( 0, 15, $coma, 0, 0, 'L' );
*/

$pdf->Ln();
$pdf->SetFont('','U',12);
$pdf->SetXY(75,55);
$nota = strtoupper("Oferta de preturi");
$pdf->Write(4,$nota);
$pdf->Ln(8);

$pdf->SetFont('Arial','',10);
/*date despre client*/
$Proprietar ="Proprietar auto: ".strtoupper($numec);
//$pdf->WriteHTML($Proprietar);
$pdf->cell(90,5,$Proprietar,1,0);
$tel ="Telefon: $tel";
$pdf->SetXY(100,63);
$pdf->cell(90,5,$tel,1,0);

$pdf->Ln(12);

/*date despre masina*/
$auto = "Marca: $marca";
$pdf->cell(80,5,$auto,1,0);
$pdf->SetXY(90,75);
$tipi ="Tip: $tip";
$pdf->cell(70,5,$tipi,1,0);
$ani ="An: $an";
$pdf->SetXY(160,75);
$pdf->cell(30,5,$ani,1,0);
$pdf->Ln(5);
$vine ="Serie sasiu: $vin";
$pdf->cell(80,5,$vine,1,0);
$pdf->SetXY(90,80);
$tipmotor = "Cm3/Kw/Tip: $serie";
$pdf->cell(70,5,$tipmotor,1,0);
$pdf->SetXY(160,80);
$nre="Nr.: $nrauto ";
$pdf->cell(30,5,$nre,1,0);

$pdf->Ln(8);
/*
$pdf->WriteHTML('SOLICITAREA CLIENTULUI:');
//$pdf->SetXY(25,120);
$pdf->Ln();
$pdf->cell(180,5,$solicit,1,0);
$pdf->Ln();
$pdf->cell(180,5,"",1,0);
$pdf->Ln();
$pdf->cell(180,5,"",1,0);
$pdf->Ln(10);
//$pdf->WriteHTML('Diagnosticare auto:');
//$pdf->SetXY(25,125);
//$pdf->WriteHTML("<blockquote>".$operatie."</blockquote>");
//$pdf->Ln(10);
* */
if($nr_operatii>0)
{
$lucrari =strtoupper("manopera:");
$pdf->WriteHTML($lucrari);
$pdf->Ln();
$pdf->cell(9,5,"Nr.",1,0,false);
//$pdf->cell(22,5,"Cod operatie",1,0,false);
$pdf->cell(118,5,"Operatie",1,0,false);
$pdf->cell(22,5,"Timp normat",1,0,false);
$pdf->cell(31,5,"Valoarea fara TVA",1,0,false);
$pdf->Ln();
$total_op=0;

	for($i=0;$i<$nr_operatii;$i++)
	{
		$pdf->cell(9,5,($i+1),1,0);
		//$pdf->cell(22,5,$cod_l[$i],1,0);
		$pdf->cell(118,5,$operati_l[$i],1,0);
		$pdf->cell(22,5,$timp_l[$i],1,0);
		$valoare_operatie=$ora_manopera*$timp_l[$i];
		$pdf->cell(31,5,$valoare_operatie,1,0);
		$total_op+=$valoare_operatie;
		$pdf->Ln();
	}
	$pdf->SETX(159,10);
	$pdf->cell(31,4,$total_op." RON",1,0);
}
/*
else{
	for($i=0;$i<7;$i++)
		{
			$pdf->cell(9,5,($i+1),1,0);
		//	$pdf->cell(22,5,"",1,0);//." ".$operati_l[$i]." ".$timp_l[$i]." ".$operator_l[$i]."<br>";
			$pdf->cell(98,5,"",1,0);
			$pdf->cell(22,5,"",1,0);
			$pdf->cell(30,5,"",1,0);
			$pdf->Ln();
		}
}//end operatiii
*/
///// `````````````````````````````````````````````````piese
$total_p=0;
if($nr_piese>0)
{
	$materiale_uti=strtoupper("Materiale:");
$pdf->Ln();
$pdf->WriteHTML($materiale_uti);
$pdf->Ln();
$pdf->cell(10,5,"Nr.",1,0,false);
//$pdf->cell(15,5,"Cod mat.",1,0,false);
$pdf->cell(104,5,"Materiale",1,0,false);
$pdf->cell(16,5,"Cantitate",1,0,false);
$pdf->cell(18,5,"Pret unitar",1,0,false);
$pdf->cell(32,5,"Valoarea fara TVA",1,0,false);
$pdf->Ln();


	for($j=0;$j<$nr_piese;$j++)
	{
			$pdf->cell(10,5,($j+1),1,0);
			//$pdf->cell(15,5,$cod_piese[$j],1,0);
			$pdf->cell(104,5,$numepiese[$j],1,0);
			$pdf->cell(16,5,$cantitate[$j],1,0);
			$pdf->cell(18,5,$valoare[$j],1,0);
			$val = $cantitate[$j]*$valoare[$j];
			$total_p+=$val;
			$pdf->cell(32,5,$val,1,0);
			$pdf->Ln();
	}
	$pdf->SETX(158,10);
	$pdf->cell(32,4,$total_p." RON",1,0);
}
///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`` end piese
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``` total
$total_tva="Total fara TVA:";
$TVAu="TVA: 24%";
$total="Total+TVA:";
$inspectie="INSPECTIE FINALA:";
$pdf->Ln(8);
$pdf->SETX(133,20);
$pdf->cell(25,5,$total_tva,1,0);
$tot_op=round($total_op+$total_p,2);
if($tot_op==0)
	$tot_op="";
$pdf->cell(32,5,$tot_op." RON",1,0);
$pdf->Ln();
$pdf->SETX(133,30);
$pdf->cell(25,5,$TVAu,1,0);
$tva = round(($tot_op)*$TVA,2);
if($tva==0)
	$tva="";
$pdf->cell(32,5,$tva." RON",1,0);
$pdf->Ln();
$pdf->SETX(133,40);
$tot=round($tot_op+$tva,2);
$pdf->cell(25,5,$total,1,0);
if($tot==0)
	$tot="";
$pdf->cell(32,5,$tot." RON",1,0);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~end total
$pdf->Ln(4);
/*
$inspectie = strtoupper("inspectie finala");
$pdf->WriteHTML($inspectie);
$pdf->Ln();
//$pdf->SetXY(25,120);
$pdf->cell(180,5,"",1,0);
$pdf->Ln();
$pdf->cell(180,5,"",1,0);
$pdf->Ln();
$pdf->cell(180,5,"",1,0);
$pdf->Ln();
$pdf->cell(180,5,"",1,0);
$pdf->Ln();
$pdf->cell(180,5,"",1,0);
$pdf->Ln(2);
$header ="CERTIFICAT DE CALITATE SI GARANTIE - Unitatea service garanteaza lucrarea executata, conform Legii nr. 449/2003, dupa cum urmeaza:";
$header2="- 3 luni de la data receptiei autovehiculului, daca lucrarea nu a necesitat inlocuiri de piese sau daca s-a executat cu piesa clientului";
$header3="- 24 luni de la data receptiei autovehiculului, pentru piese furnizate de unitate, conditionat de respectarea indicatiilor de exploatare prevazute de";
$header4=" producatorul autovehiculului.";
* */
$semnatura_s =strtoupper("reprezentant service");
$semnatura_c =strtoupper("client");
/*
$pdf->Ln(5);
$pdf->SetFont('Arial','',8);
$pdf->cell(10,5,$header,0,0);
$pdf->Ln();
$pdf->cell(10,5,$header2,0,0);
$pdf->Ln();
$pdf->cell(10,5,$header3,0,0);
$pdf->Ln();
$pdf->cell(10,5,$header4,0,0);
/*$pdf->setHeader('color','blue');
$pdf->setFooter('left',$header);*/
$pdf->Ln(5);
//$pdf->SetXY(10,258);
/*
$pdf->cell(20,5,$semnatura_s,0,0,false);
$pdf->cell(20,5,"",0,0,false);
$pdf->cell(100,5,"",0,0,false);
$pdf->cell(20,5,"",0,0,false);
//$pdf->SetXY(170,258);
$pdf->cell(30,5,$semnatura_c,0,0,false);
*/
$pdf->Output("$nrauto.pdf",'D');                         // Trimite datele de iesire la browser
$pdf->close();
}
?> 

