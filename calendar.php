Code: calendar.php
<!-- <body> -->

<div id=\"overDiv\" style=\"position:absolute; visibility:hide;z-index:1000;\"></div>
<SCRIPT LANGUAGE=\"JavaScript\" SRC=\"overlib.js\"></script>

<? 
/* Constructed by Jonathan Crawford for Riverside Bible Church on 8-14-2001. This calendar is set up to use OverLIB and MySQL.  I created tables for each month in MySQL with columns \"date\", \"who\", \"what\", \"weyr\", and \"wen\".  I check to see if the date is equal to any entries in the \"date\" column of the current month, then display \"who\" in the calendar.  I use OverLIB to display the \"what\", \"weyr\" and \"wen\" values.  You can see this in use at:
http://www.riversidebiblechurch.org/
thanks to Seamus Venasse, for posting the basic construct of the calendar at px.sklar.com 
*/

/* Change to month/year to display */ 
$month = date(\"n\"); 
$year = date(\"Y\");
$day = date(\"j\"); 

/* Display the title of the month on the calendar */
$month_title[] = \"January\";
$month_title[] = \"February\";
$month_title[] = \"March\";
$month_title[] = \"April\";
$month_title[] = \"May\";
$month_title[] = \"June\";
$month_title[] = \"July\";
$month_title[] = \"August\";
$month_title[] = \"September\";
$month_title[] = \"October\";
$month_title[] = \"November\";
$month_title[] = \"December\";

/* Display the titles on the header of the calendar */ 
$week_titles[] = \"Sun\"; 
$week_titles[] = \"Mon\"; 
$week_titles[] = \"Tue\"; 
$week_titles[] = \"Wed\"; 
$week_titles[] = \"Thu\"; 
$week_titles[] = \"Fri\"; 
$week_titles[] = \"Sat\"; 

/* determine total number of days in a month */ 
$totaldays = 0; 
while ( checkdate( $month, $totaldays + 1, $year ) ) 
        $totaldays++; 

/* build table */ 
echo \'<table border=\"1\" cellpadding=\"5\" cellspacing=\"5\" width=\"100%\"><tr>\';  
echo \'<td colspan=7 bgcolor=red align=center><h1>\', $month_title[--$month], \'</h1></td></tr><tr>\';
for ( $x = 0; $x < 7; $x++ ) 
        echo \'<th width=14%><b><font face=\"Arial\">\', $week_titles[ $x ], \'</font></b></th>\'; 

/* ensure that a number of blanks are put in so that the first day of the month 
   lines up with the proper day of the week */ 
$offset = date( \"w\", \"U\" ); 
echo \'<tr>\'; 
if ( $offset > 0 ) 
        echo str_repeat( \"<td width=14% height=70> </td>\", $offset ); 

/* start entering in the information */ 

require(\"$DOCUMENT_ROOT/../dbconnect.inc\"); /* connect to MySQL database */

$currmonth = $month_title[$month];

for ( $day = 1; $day <= $totaldays; $day++ ) 
{ 
    $query = \"select * from \".$currmonth.\" where date = \".$day;
        $result = mysql_query($query);
        $num_results = mysql_num_rows($result); 

        if ( $day == date(\"j\") ) /* check if this is today, highlight box */
	{
            echo \'<td width=14% bgcolor=\"#FFFFCC\" height=70><table><tr><td align=left valign=top>\'.$day.\'</td></tr></table>\';
            for ($i=0;$i<$num_results;$i++)
            {
                $row = mysql_fetch_array($result);

                echo \'<a href=\"javascript:void(0);\" onMouseOver=\"return overlib(\\\'\';
                echo $row[\"what\"].\'<br>\'.$row[\"weyr\"].\'<br>\'.$row[\"wen\"];
                echo \'\\\', LEFT)\" onMouseOut=\"nd()\">\';
                echo htmlspecialchars( stripslashes($row[\"who\"])).\'</a><br>\';
            }
            echo \'</td>\';
	    $offset++;
	}
	else 
	{
	    echo \'<td width=14% height=70><table><tr><td align=left valign=top>\', $day, \'</td></tr></table>\';
            for ($i=0;$i<$num_results;$i++)
            {
                $row = mysql_fetch_array($result);
                echo \'<a href=\"javascript:void(0);\" onMouseOver=\"return overlib(\\\'\';
                echo $row[\"what\"].\'<br>\'.$row[\"weyr\"].\'<br>\'.$row[\"wen\"];
                echo \'\\\', LEFT)\" onMouseOut=\"nd()\">\';

                echo htmlspecialchars( stripslashes($row[\"who\"])).\'</a><br>\';
            }
            echo \'</td>\'; 
            $offset++; 
	}
	/* if we\'re on the last day of the week, wrap to the other side */ 
        if ( $offset > 6 ) 
        { 
                $offset = 0; 
                echo \'</tr>\'; 
                if ( $day < $totaldays ) 
                        echo \'<tr>\'; 
        } 
} 

/* fill in the remaining spaces for the end of the month, just to make it look 
   pretty */ 
if ( $offset > 0 ) 
        $offset = 7 - $offset; 

if ( $offset > 0 ) 
        echo str_repeat( \"<td width=14% height=70> </td>\", $offset ); 

/* end the table */ 
echo \'</td></table>\'; 

?> 


